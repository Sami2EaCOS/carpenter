package me.smourad.carpentry.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.FaceAttachable;
import org.bukkit.block.data.type.Switch;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Transformation;
import org.jetbrains.annotations.NotNull;
import org.joml.Vector3f;

import java.util.Collection;
import java.util.List;

public class CarpentryLever extends CarpentryBlock {

    public CarpentryLever(Block block, Material material) {
        super(block, material);
    }

    public CarpentryLever(Block block, List<BlockDisplay> display) {
        super(block, display);
    }

    @Override
    protected Collection<BoundingBox> getBoundingBoxes() {
        return List.of(block.getBoundingBox());
    }

    @Override
    protected @NotNull Transformation calculateTransformation(BoundingBox bb) {
        Switch data = (Switch) block.getBlockData();
        FaceAttachable.AttachedFace face = data.getAttachedFace();
        Transformation transformation = super.calculateTransformation(bb);
        Vector3f translation = transformation.getTranslation();
        translation.sub(block.getX(), block.getY(), block.getZ());
        Vector3f scale = transformation.getScale();

        switch (face) {
            case WALL -> {
                switch (data.getFacing()) {
                    case NORTH -> {
                        scale.div(1, 1, 2);
                        translation.add(0, 0, scale.z);
                    }
                    case SOUTH -> scale.div(1, 1, 2);
                    case EAST -> scale.div(2, 1, 1);
                    case WEST -> {
                        scale.div(2, 1, 1);
                        translation.add(scale.x, 0, 0);
                    }
                }
            }
            case FLOOR -> scale.div(1, 2, 1);
            case CEILING -> {
                scale.div(1, 2, 1);
                translation.add(0, scale.y, 0);
            }
        }

        return transformation;
    }

}
