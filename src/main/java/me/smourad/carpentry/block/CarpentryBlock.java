package me.smourad.carpentry.block;

import me.smourad.carpentry.CarpentryUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.EntityType;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Transformation;
import org.jetbrains.annotations.NotNull;
import org.joml.AxisAngle4f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class CarpentryBlock {

    public static final String CARPENTRY_BASE = "CarpentryBase";

    protected final Block block;
    protected final Material material;
    protected List<BlockDisplay> display;

    public CarpentryBlock(
            Block block,
            Material material
    ) {
        this.block = block;
        this.material = material;
        this.display = List.of();
    }

    public CarpentryBlock(Block block, List<BlockDisplay> display) {
        this.block = block;
        this.material = display.get(0).getBlock().getMaterial();
        this.display = display;
    }

    public void update() {
        Location location = block.getLocation();

        List<BlockDisplay> trash = new ArrayList<>(display);
        List<BlockDisplay> newDisplay = new ArrayList<>();

        List<BoundingBox> bbs = getBoundingBoxes().stream().toList();
        for (int i = 0; i < bbs.size(); i++) {
            BoundingBox bb = bbs.get(i);

            Transformation transformation = calculateTransformation(bb);
            BlockDisplay bd = CarpentryUtils.harvest(display, i);
            if (Objects.isNull(bd)) {
                bd = (BlockDisplay) block.getWorld().spawnEntity(location, EntityType.BLOCK_DISPLAY);
                PersistentDataContainer persistentDataContainer = bd.getPersistentDataContainer();
                persistentDataContainer.set(CarpentryUtils.CARPENTER_KEY, PersistentDataType.INTEGER, i);
            }

            bd.teleport(block.getLocation());
            bd.setTransformation(transformation);
            bd.setBlock(material.createBlockData());

            trash.remove(bd);
            newDisplay.add(bd);
        }

        trash.forEach(BlockDisplay::remove);
        display = newDisplay;
    }

    protected Collection<BoundingBox> getBoundingBoxes() {
        return block.getCollisionShape().getBoundingBoxes();
    }

    @NotNull
    protected Transformation calculateTransformation(BoundingBox bb) {
        float x = (float) (bb.getWidthX() + 0.005f);
        float y = (float) (bb.getHeight() + 0.005f);
        float z = (float) (bb.getWidthZ() + 0.005f);

        float centerX = (float) bb.getMinX();
        float centerY = (float) bb.getMinY();
        float centerZ = (float) bb.getMinZ();

        return new Transformation(
                new Vector3f(centerX - 0.0025f, centerY - 0.0025f, centerZ - 0.0025f),
                new AxisAngle4f(),
                new Vector3f(x, y, z),
                new AxisAngle4f()
        );
    }

}
