package me.smourad.carpentry.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Transformation;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public class CarpentryButton extends CarpentryBlock {

    public CarpentryButton(Block block, Material material) {
        super(block, material);
    }

    public CarpentryButton(Block block, List<BlockDisplay> display) {
        super(block, display);
    }

    @Override
    protected Collection<BoundingBox> getBoundingBoxes() {
        return List.of(block.getBoundingBox());
    }

    @Override
    protected @NotNull Transformation calculateTransformation(BoundingBox bb) {
        Transformation transformation = super.calculateTransformation(bb);
        transformation.getTranslation().sub(block.getX(), block.getY(), block.getZ());
        return transformation;
    }

}
