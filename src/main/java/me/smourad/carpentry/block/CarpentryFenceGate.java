package me.smourad.carpentry.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.type.Gate;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Transformation;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;

public class CarpentryFenceGate extends CarpentryBlock {

    public CarpentryFenceGate(Block block, Material material) {
        super(block, material);
    }

    public CarpentryFenceGate(Block block, List<BlockDisplay> display) {
        super(block, display);
    }

    @Override
    protected Collection<BoundingBox> getBoundingBoxes() {
        BoundingBox base = block.getBoundingBox();
        Gate gate = (Gate) block.getBlockData();
        Vector location = block.getLocation().toVector();
        Vector min = base.getMin();
        Vector max = base.getMax();
        Vector divider;
        Vector threshold = max.clone().subtract(location).setY(0);

        switch (gate.getFacing()) {
            case EAST, WEST -> {
                divider = new Vector(1, 1, 2);
                threshold.setX(0);
            }
            case NORTH, SOUTH -> {
                divider = new Vector(2, 1, 1);
                threshold.setZ(0);
            }
            default -> throw new RuntimeException("No Fence Facing");
        }

        threshold.divide(divider);
        BoundingBox door1 = BoundingBox.of(min.clone(), max.clone().subtract(threshold));
        BoundingBox door2 = BoundingBox.of(min.clone().add(threshold), max.clone());

        if (gate.isOpen()) {
            Vector beginDoor1;
            Vector beginDoor2;

            switch (gate.getFacing()) {
                case SOUTH -> {
                    beginDoor1 = min.clone()
                            .add(new Vector(0, 0.5, 0))
                            .add(new Vector(0, 0, 0.3125));
                    beginDoor2 = max.clone()
                            .subtract(new Vector(0, 0.5, 0))
                            .add(new Vector(0, 0, 0.0625));
                }
                case WEST -> {
                    beginDoor1 = min.clone()
                            .add(new Vector(0, 0.5, 0))
                            .subtract(new Vector(0.0625, 0, 0));
                    beginDoor2 = max.clone()
                            .subtract(new Vector(0, 0.5, 0))
                            .subtract(new Vector(0.3125, 0, 0));
                }
                case NORTH -> {
                    beginDoor1 = min.clone()
                            .add(new Vector(0, 0.5, 0))
                            .subtract(new Vector(0, 0, 0.0625));
                    beginDoor2 = max.clone()
                            .subtract(new Vector(0, 0.5, 0))
                            .subtract(new Vector(0, 0, 0.3125));
                }
                case EAST -> {
                    beginDoor1 = min.clone()
                            .add(new Vector(0, 0.5, 0))
                            .add(new Vector(0.3125, 0, 0));
                    beginDoor2 = max.clone()
                            .subtract(new Vector(0, 0.5, 0))
                            .add(new Vector(0.0625, 0, 0));
                }
                default -> throw new RuntimeException("No Fence Facing");
            }

            door1 = BoundingBox.of(beginDoor1, door1.getWidthZ() / 2, door1.getHeight() / 2, door1.getWidthX() / 2);
            door2 = BoundingBox.of(beginDoor2, door2.getWidthZ() / 2, door2.getHeight() / 2, door2.getWidthX() / 2);
        }

        return List.of(door1, door2);
    }

    @Override
    protected @NotNull Transformation calculateTransformation(BoundingBox bb) {
        Transformation transformation = super.calculateTransformation(bb);
        transformation.getTranslation().sub(block.getX(), block.getY(), block.getZ());
        return transformation;
    }

}
