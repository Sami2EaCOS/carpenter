package me.smourad.carpentry.block;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Transformation;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class CarpentryFence extends CarpentryBlock {

    public CarpentryFence(Block block, Material material) {
        super(block, material);
    }

    public CarpentryFence(Block block, List<BlockDisplay> display) {
        super(block, display);
    }

    @Override
    protected @NotNull Transformation calculateTransformation(BoundingBox bb) {
        Transformation transformation = super.calculateTransformation(bb);
        transformation.getScale().sub(0, 0.5f, 0);
        return transformation;
    }

}
