package me.smourad.carpentry;

import org.bukkit.plugin.java.JavaPlugin;

public class CarpentryPlugin extends JavaPlugin {

    public static CarpentryPlugin INSTANCE;

    @Override
    public void onEnable() {
        INSTANCE = this;

        Carpenter carpenter = new Carpenter(this);
        getServer().getPluginManager().registerEvents(carpenter, this);
    }

}
