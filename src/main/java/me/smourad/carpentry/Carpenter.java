package me.smourad.carpentry;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.util.Location;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.Flags;
import me.smourad.carpentry.block.*;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class Carpenter implements Listener {

    private final CarpentryPlugin plugin;

    public Carpenter(CarpentryPlugin plugin) {
        this.plugin = plugin;
    }

    protected boolean hasWorldGuardPermissionToInteract(Player player, Block block) {
        try {
            LocalPlayer p = WorldGuardPlugin.inst().wrapPlayer(player);
            Location loc = BukkitAdapter.adapt(block.getLocation());
            World w = BukkitAdapter.adapt(block.getWorld());
            boolean canBuild = WorldGuard.getInstance().getPlatform()
                    .getRegionContainer()
                    .createQuery()
                    .testState(loc, p, Flags.BUILD);
            boolean hasBypass = WorldGuard.getInstance().getPlatform().getSessionManager().hasBypass(p, w);

            return canBuild || hasBypass;
        } catch (NoClassDefFoundError e) {
            return true;
        }
    }

    @EventHandler
    public void onSetup(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        if (Objects.isNull(block) || !CarpentryUtils.isPossibleCarpentryBlock(block)) return;
        if (!hasWorldGuardPermissionToInteract(player, block)) return;

        if (Objects.equals(Action.RIGHT_CLICK_BLOCK, event.getAction()) && player.isSneaking()) {
            ItemStack item = event.getItem();

            if (Objects.nonNull(item) && CarpentryUtils.isPossibleCarpentryMaterial(item.getType())) {
                Material blockType = block.getType();
                Material type = item.getType();

                if (Objects.equals(blockType, type)) return;

                if (!Objects.equals(player.getGameMode(), GameMode.CREATIVE)) {
                    item.setAmount(item.getAmount() - 1);
                    EquipmentSlot hand = event.getHand();
                    assert hand != null;
                    player.getInventory().setItem(hand, item);

                    remove(block, true);
                } else {
                    remove(block, false);
                }

                create(block, type);
                event.setCancelled(true);
            }
        }
    }

    protected void create(Block block, Material type) {
        switch (block.getType()) {
            case LEVER:
                new CarpentryLever(block, type).update();
                break;
            case SNOW:
                new CarpentrySnow(block, type).update();
                break;
            case NETHER_BRICK_WALL:
            case ANDESITE_WALL:
            case BLACKSTONE_WALL:
            case BRICK_WALL:
            case COBBLESTONE_WALL:
            case COBBLED_DEEPSLATE_WALL:
            case DEEPSLATE_BRICK_WALL:
            case DIORITE_WALL:
            case DEEPSLATE_TILE_WALL:
            case GRANITE_WALL:
            case END_STONE_BRICK_WALL:
            case MOSSY_COBBLESTONE_WALL:
            case MUD_BRICK_WALL:
            case MOSSY_STONE_BRICK_WALL:
            case POLISHED_BLACKSTONE_BRICK_WALL:
            case POLISHED_BLACKSTONE_WALL:
            case PRISMARINE_WALL:
            case POLISHED_DEEPSLATE_WALL:
            case RED_NETHER_BRICK_WALL:
            case SANDSTONE_WALL:
            case RED_SANDSTONE_WALL:
            case STONE_BRICK_WALL:
                new CarpentryWall(block, type).update();
                break;
            case ACACIA_FENCE:
            case BAMBOO_FENCE:
            case BIRCH_FENCE:
            case CHERRY_FENCE:
            case CRIMSON_FENCE:
            case DARK_OAK_FENCE:
            case JUNGLE_FENCE:
            case MANGROVE_FENCE:
            case NETHER_BRICK_FENCE:
            case OAK_FENCE:
            case SPRUCE_FENCE:
            case WARPED_FENCE:
                new CarpentryFence(block, type).update();
                break;
            case ACACIA_FENCE_GATE:
            case BAMBOO_FENCE_GATE:
            case BIRCH_FENCE_GATE:
            case CHERRY_FENCE_GATE:
            case CRIMSON_FENCE_GATE:
            case DARK_OAK_FENCE_GATE:
            case JUNGLE_FENCE_GATE:
            case MANGROVE_FENCE_GATE:
            case OAK_FENCE_GATE:
            case SPRUCE_FENCE_GATE:
            case WARPED_FENCE_GATE:
                new CarpentryFenceGate(block, type).update();
                break;
            case DARK_OAK_BUTTON:
            case ACACIA_BUTTON:
            case BAMBOO_BUTTON:
            case BIRCH_BUTTON:
            case CHERRY_BUTTON:
            case CRIMSON_BUTTON:
            case JUNGLE_BUTTON:
            case MANGROVE_BUTTON:
            case OAK_BUTTON:
            case SPRUCE_BUTTON:
            case WARPED_BUTTON:
            case POLISHED_BLACKSTONE_BUTTON:
            case STONE_BUTTON:
                new CarpentryButton(block, type).update();
                break;
            case DARK_OAK_PRESSURE_PLATE:
            case ACACIA_PRESSURE_PLATE:
            case BAMBOO_PRESSURE_PLATE:
            case BIRCH_PRESSURE_PLATE:
            case CHERRY_PRESSURE_PLATE:
            case CRIMSON_PRESSURE_PLATE:
            case JUNGLE_PRESSURE_PLATE:
            case MANGROVE_PRESSURE_PLATE:
            case OAK_PRESSURE_PLATE:
            case SPRUCE_PRESSURE_PLATE:
            case WARPED_PRESSURE_PLATE:
            case POLISHED_BLACKSTONE_PRESSURE_PLATE:
            case STONE_PRESSURE_PLATE:
            case HEAVY_WEIGHTED_PRESSURE_PLATE:
            case LIGHT_WEIGHTED_PRESSURE_PLATE:
                new CarpentryPressurePlate(block, type).update();
                break;
            default:
                new CarpentryBlock(block, type).update();
                break;
        }
    }

    @EventHandler
    public void onBreak(BlockBreakEvent event) {
        Block block = event.getBlock();
        if (!CarpentryUtils.isPossibleCarpentryBlock(block)) return;

        remove(block, true);
    }

    @EventHandler
    public void onRemove(PlayerInteractEvent event) {
        Player player = event.getPlayer();

        Block block = event.getClickedBlock();
        if (Objects.isNull(block) || !CarpentryUtils.isPossibleCarpentryBlock(block)) return;
        if (!hasWorldGuardPermissionToInteract(player, block)) return;

        if (Objects.equals(Action.RIGHT_CLICK_BLOCK, event.getAction())
                && player.isSneaking()
                && (Objects.isNull(event.getItem()) || Objects.equals(event.getItem().getType(), Material.AIR))
        ) {
            remove(block, !Objects.equals(player.getGameMode(), GameMode.CREATIVE));
        }
    }

    protected void remove(Block block, boolean drop) {
        Collection<BlockDisplay> display = block.getWorld()
                .getNearbyEntitiesByType(BlockDisplay.class, block.getLocation(), 1);
        List<BlockDisplay> shapers = CarpentryUtils.harvest(display);

        remove(block, shapers, drop);
    }

    protected void remove(Block block, List<BlockDisplay> shapers, boolean drop) {
        if (!shapers.isEmpty()) {
            Material material = shapers.get(0).getBlock().getMaterial();

            if (drop) {
                block.getWorld().dropItemNaturally(block.getLocation(), new ItemStack(material));
            }

            shapers.forEach(BlockDisplay::remove);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Block block = event.getClickedBlock();
        if (Objects.isNull(block)) return;

        new BukkitRunnable() {
            @Override
            public void run() {
                update(block);
            }
        }.runTaskLater(plugin, 0L);
    }

    @EventHandler
    public void onUpdate(BlockPhysicsEvent event) {
        Block block = event.getBlock();
        update(block);
    }

    protected void update(Block block) {
        Collection<BlockDisplay> display = block.getWorld().getNearbyEntitiesByType(BlockDisplay.class, block.getLocation(), 1);
        List<BlockDisplay> shapers = CarpentryUtils.harvest(display);

        update(block, shapers);
    }

    protected void update(Block block, List<BlockDisplay> shapers) {
        if (!shapers.isEmpty()) {
            Material type = block.getType();

            switch (type) {
                case AIR:
                case MOVING_PISTON:
                case WATER:
                case LAVA:
                    remove(block, shapers, true);
                    break;
                case LEVER:
                    new CarpentryLever(block, shapers).update();
                    break;
                case SNOW:
                    new CarpentrySnow(block, shapers).update();
                    break;
                case NETHER_BRICK_WALL:
                case ANDESITE_WALL:
                case BLACKSTONE_WALL:
                case BRICK_WALL:
                case COBBLESTONE_WALL:
                case COBBLED_DEEPSLATE_WALL:
                case DEEPSLATE_BRICK_WALL:
                case DIORITE_WALL:
                case DEEPSLATE_TILE_WALL:
                case GRANITE_WALL:
                case END_STONE_BRICK_WALL:
                case MOSSY_COBBLESTONE_WALL:
                case MUD_BRICK_WALL:
                case MOSSY_STONE_BRICK_WALL:
                case POLISHED_BLACKSTONE_BRICK_WALL:
                case POLISHED_BLACKSTONE_WALL:
                case PRISMARINE_WALL:
                case POLISHED_DEEPSLATE_WALL:
                case RED_NETHER_BRICK_WALL:
                case SANDSTONE_WALL:
                case RED_SANDSTONE_WALL:
                case STONE_BRICK_WALL:
                    new CarpentryWall(block, shapers).update();
                    break;
                case ACACIA_FENCE:
                case BAMBOO_FENCE:
                case BIRCH_FENCE:
                case CHERRY_FENCE:
                case CRIMSON_FENCE:
                case DARK_OAK_FENCE:
                case JUNGLE_FENCE:
                case MANGROVE_FENCE:
                case NETHER_BRICK_FENCE:
                case OAK_FENCE:
                case SPRUCE_FENCE:
                case WARPED_FENCE:
                    new CarpentryFence(block, shapers).update();
                    break;
                case ACACIA_FENCE_GATE:
                case BAMBOO_FENCE_GATE:
                case BIRCH_FENCE_GATE:
                case CHERRY_FENCE_GATE:
                case CRIMSON_FENCE_GATE:
                case DARK_OAK_FENCE_GATE:
                case JUNGLE_FENCE_GATE:
                case MANGROVE_FENCE_GATE:
                case OAK_FENCE_GATE:
                case SPRUCE_FENCE_GATE:
                case WARPED_FENCE_GATE:
                    new CarpentryFenceGate(block, shapers).update();
                    break;
                case DARK_OAK_BUTTON:
                case ACACIA_BUTTON:
                case BAMBOO_BUTTON:
                case BIRCH_BUTTON:
                case CHERRY_BUTTON:
                case CRIMSON_BUTTON:
                case JUNGLE_BUTTON:
                case MANGROVE_BUTTON:
                case OAK_BUTTON:
                case SPRUCE_BUTTON:
                case WARPED_BUTTON:
                case POLISHED_BLACKSTONE_BUTTON:
                case STONE_BUTTON:
                    new CarpentryButton(block, shapers).update();
                    break;
                case DARK_OAK_PRESSURE_PLATE:
                case ACACIA_PRESSURE_PLATE:
                case BAMBOO_PRESSURE_PLATE:
                case BIRCH_PRESSURE_PLATE:
                case CHERRY_PRESSURE_PLATE:
                case CRIMSON_PRESSURE_PLATE:
                case JUNGLE_PRESSURE_PLATE:
                case MANGROVE_PRESSURE_PLATE:
                case OAK_PRESSURE_PLATE:
                case SPRUCE_PRESSURE_PLATE:
                case WARPED_PRESSURE_PLATE:
                case POLISHED_BLACKSTONE_PRESSURE_PLATE:
                case STONE_PRESSURE_PLATE:
                case HEAVY_WEIGHTED_PRESSURE_PLATE:
                case LIGHT_WEIGHTED_PRESSURE_PLATE:
                    new CarpentryPressurePlate(block, shapers).update();
                    break;
                default:
                    new CarpentryBlock(block, shapers).update();
                    break;
            }
        }
    }

}
